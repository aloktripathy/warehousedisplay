﻿using System.Web.Mvc;
using WarehouseDisplay.WarehouseService;

namespace WarehouseDisplay.Controllers
{
    public class HomeController : Controller
    {
        readonly IWarehouseService _warehouseService = new WarehouseServiceClient();

        public ActionResult Index()
        {
            var warehouses = _warehouseService.GetWarehouses();

            return View(warehouses);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}