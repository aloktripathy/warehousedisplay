﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WarehouseDisplay.Startup))]
namespace WarehouseDisplay
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
